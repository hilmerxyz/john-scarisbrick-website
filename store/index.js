import Vuex from 'vuex'
import {createClient} from '@/plugins/contentful'

const createStore = () => {
  return new Vuex.Store({
    state: {
      sideBar: false,
      sideBarData: {noResult: false},
      pageName: '',
      pixelRatio: undefined
    },
    mutations: {
      closeSideBar(state) {
        state.sideBar = false
      },
      openSideBar(state) {
        state.sideBar = true
      },
      setSideBarData (state, payload) {
        state.sideBarData = payload
      },
      setPageName (state, payload) {
        state.pageName = payload
      },
      setPixelRatio (state, payload) {
        state.pixelRatio = payload
      }
    },
    getters: {
      isSideBarOpen: (state) => {
        return state.sideBar
      },
      getSideBarData: (state) => {
        return state.sideBarData
      },
      getPageName: (state) => {
        return state.pageName
      },
      getPixelRatio: (state) => {
        return state.pixelRatio
      }
    },
    actions: {
      nuxtServerInit({ commit }) {
        let client = createClient()

        return client.getEntries({
          content_type: 'menu'
        })
          .then(item => {
            return {
              about: item.items[0].fields.about,
              galleries: item.items[0].fields.galleries.map(item => item.fields.title),
              links: item.items[0].fields.linksLinks.map(item => item.fields)
            }
          })
          .then(result => {
            commit('setSideBarData', result)
            return result
          })
      }
    }
  })
}

export default createStore
