const pkg = require('./package')
const resolve = require('path').resolve

const {createClient} = require('./plugins/contentful')
let client = createClient()



createClient()

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'title', content: 'John Scarisbrick - Photographer' },
      { hid: 'description', name: 'description', content: 'John Scarisbrick' },
      { property: 'og:title', content: 'John Scarisbrick' },
      { property: 'og:description', content: 'Photographer' },
      { property: 'og:image', content: 'https://johnscarisbrick.netlify.com/js.png?123' },
      { property: 'og:image:width', content: '1200' },
      { property: 'og:image:height', content: '630' },
      { property: 'twitter:card', content: 'summary_large_image' },
      { property: 'twitter:description', content: 'Photographer' },
      { property: 'twitter:title', content: 'John Scarisbrick' },
      { property: 'twitter:image', content: 'https://johnscarisbrick.netlify.com/js.png?123' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon-js.ico' }
    ]
  },


  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#FFFFFF' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/main.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vueScroll',
    '~/plugins/vueScrollTo'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    ['nuxt-sass-resources-loader', [
      resolve(__dirname, 'assets/scss/file/media-queries.scss'),
      resolve(__dirname, 'assets/scss/file/_path-two.scss')
    ]]
  ],

  generate: {
    routes () {
      return Promise.all([
        client.getEntries({
          'content_type': 'galleryPage'
        })
      ])
        .then(([entries]) => {
          return [
            ...entries.items.map(entry => `/gallery-${entry.fields.title.toLowerCase()}`),
          ]
        })
    }
  },

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    postcss: {
      plugins: {
        'postcss-cssnext': {
          features: {
            customProperties: false
          }
        }
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    }
  },
  vendor:[
    'firebase'
  ]
}
