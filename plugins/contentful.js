const contentful = require('contentful')
const defaultConfig = {
  CTF_BLOG_POST_TYPE_ID: 'galleryPage',
  CTF_SPACE_ID: 'ivjwcpjf2lqf',
  CTF_CDA_ACCESS_TOKEN: 'b6b3ddf246b70c949af516524fb18494370586c88da9f3414edce949478ccd81',
}

module.exports = {
  createClient (config = defaultConfig) {
    return contentful.createClient({
      space: config.CTF_SPACE_ID,
      accessToken: config.CTF_CDA_ACCESS_TOKEN,
      contentType: config.CTF_BLOG_POST_TYPE_ID
    })
  }
}
